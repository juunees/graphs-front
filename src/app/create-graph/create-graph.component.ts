import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-create-graph',
  templateUrl: './create-graph.component.html',
  styleUrls: ['./create-graph.component.css']
})
export class CreateGraphComponent implements OnInit {

  constructor(private httpService: HttpService , private router: Router) { }

  ngOnInit() {
  }

  onCreate(name) {

    const data = {name : name };
    this.httpService.post('graph/creategraph', data).subscribe(value => {
        console.log(value);
        this.router.navigate(['graphs']);
      },
      error => {
        console.log(error);
      });
  }

}
