import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { GraphComponent } from './graph/graph.component';
import { RouterModule, Routes } from '@angular/router';
import { CreateGraphComponent } from './create-graph/create-graph.component';
import { HttpClientModule } from '@angular/common/http';
import {HttpService} from './services/http.service';
import { GraphsComponent } from './graphs/graphs.component';
import {FormsModule} from '@angular/forms';
import {SocketService} from './services/socket.service';


const appRoutes: Routes = [
  { path: 'graph/:id', component: GraphComponent },
  { path: 'create' , component: CreateGraphComponent},
  { path: 'graphs', component: GraphsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    CreateGraphComponent,
    GraphsComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    HttpService,
    SocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
