import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class HttpService {
   apiRoot;
  constructor(private httpClient: HttpClient) {
    this.apiRoot = 'http://127.0.0.1:8080/';
  }
  public post(url: string, data): Observable<any> {
    return this.httpClient.post(this.apiRoot + url, data);
  }

  public get(url: string): Observable<any> {
    return this.httpClient.get(this.apiRoot + url);
  }

}
