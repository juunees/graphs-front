import * as io from 'socket.io-client';
import {Observable} from 'rxjs';

export class SocketService {
  private url = 'http://localhost:3000';
  private socket;

  constructor() {
    this.socket = io(this.url);
  }

  public reload() {
    this.socket.emit('reload');
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('reload', (message) => {
        observer.next(message);
      });
    });
  }

}
