import { Component, OnInit } from '@angular/core';
import {HttpService} from '../services/http.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.component.html',
  styleUrls: ['./graphs.component.css']
})
export class GraphsComponent implements OnInit {

  public graphs;
  constructor(private httpService: HttpService , private router: Router) { }

  ngOnInit() {
    this.httpService.get('graph/getgraphs').subscribe(value => {
        console.log(value);
        this.graphs = value;
      },
      error => {
        console.log(error);
      });
  }

  onDelete(id) {
    console.log('delete graph' + id);
    const data = {id : id };
    this.httpService.post('graph/deletegraph', data).subscribe(value => {
        console.log(value);
        location.reload();
      },
      error => {
        console.log(error);
      });
  }

  onOpen(id) {
    console.log('delete graph' + id);
    this.router.navigate(['graph', id]);
  }

}
