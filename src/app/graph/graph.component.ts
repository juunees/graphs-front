import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as d3 from 'd3';
import {HttpService} from '../services/http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SocketService} from '../services/socket.service';

interface Node {
  id: string;
  group: number;
}

interface Link {
  source: string;
  target: string;
  value: number;
}

interface Graph {
  nodes: Node[];
  links: Link[];
}

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class GraphComponent implements OnInit {
  public graphId;
  public findResult;

  constructor(private httpService: HttpService, private route: ActivatedRoute, private socket: SocketService) {
    this.route.params.subscribe( params => this.graphId = params.id );
  }

  ngOnInit() {

    console.log(this.graphId);
    const data = {id : this.graphId };
    this.httpService.post('graph/getgraph', data).subscribe(value => {
        console.log(value);
        const dataGraph = this.parseGraph(value);
        this.setGraph(dataGraph);
      },
      error => {
        console.log(error);
      });
  }

  onCreateVertex(name) {

    const data = {name : name, graphId : this.graphId };
    this.httpService.post('/vertex/createvertex', data).subscribe(value => {
        console.log(value);
         this.socket.reload();
      },
      error => {
        console.log(error);
      });
  }

  onCreatePath(v1, v2, weight) {

    const data = {from : v1, to: v2, weight: weight, graphId : this.graphId };
    this.httpService.post('/path/createpath', data).subscribe(value => {
        console.log(value);
        this.socket.reload();
      },
      error => {
        console.log(error);
      });
  }

  onFindPath(v1, v2, weight) {

    const data = {from : v1, to: v2, graphId : this.graphId };
    this.httpService.post('/path/findpath', data).subscribe(value => {
        console.log(value);
        this.findResult = value;
      },
      error => {
        console.log(error);
      });
  }

  setGraph(data) {
    const svg = d3.select('svg');
    const width = +svg.attr('width');
    const height = +svg.attr('height');

    const color = d3.scaleOrdinal(d3.schemeCategory20);

    const simulation = d3.forceSimulation()
      .force('link', d3.forceLink().id((d: any) => d.id))
      .force('charge', d3.forceManyBody())
      .force('center', d3.forceCenter(width / 2, height / 2));

      const nodes: Node[] = [];
      const links: Link[] = [];

      data.nodes.forEach((d) => {
        nodes.push(<Node>d);
      });

      data.links.forEach((d) => {
        links.push(<Link>d);
      });
      const graph: Graph = <Graph>{ nodes, links };

      const link = svg.append('g')
        .attr('class', 'links')
        .selectAll('line')
        .data(graph.links)
        .enter()
        .append('line')
        .attr('stroke-width', (d: any) => Math.sqrt(d.value));

      const node = svg.append('g')
        .attr('class', 'nodes')
        .selectAll('circle')
        .data(graph.nodes)
        .enter()
        .append('circle')
        .attr('r', 5)
        .attr('fill', (d: any) => color(d.group));


      svg.selectAll('circle').call(d3.drag()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended)
      );

      node.append('title')
        .text((d) => d.id);

      link.append('title')
        .text((d) => d.value);

      simulation
        .nodes(graph.nodes)
        .on('tick', ticked);

      simulation.force<d3.ForceLink<any, any>>('link')
        .links(graph.links);

      function ticked() {
        link
          .attr('x1', function(d: any) { return d.source.x; })
          .attr('y1', function(d: any) { return d.source.y; })
          .attr('x2', function(d: any) { return d.target.x; })
          .attr('y2', function(d: any) { return d.target.y; });

        node
          .attr('cx', function(d: any) { return d.x; })
          .attr('cy', function(d: any) { return d.y; });
      }

    function dragstarted(d) {
      if (!d3.event.active) { simulation.alphaTarget(0.3).restart(); }
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) { simulation.alphaTarget(0); }
      d.fx = null;
      d.fy = null;
    }
  }

  parseGraph(value){

    const nodes = value.vertices;
    const links = [];

    value.paths.forEach(function (value) {
       links.push({'source' : value.from , 'target' : value.to, 'value' : value.weight});
    });

    const dataGraph = {
      'nodes' : nodes,
      'links' : links
    }

    // const dataGraph = {
    //   'nodes': [
    //     {'id': 'A'},
    //     {'id': 'B'}
    //   ],
    //   'links': [
    //     {'source': 'A', 'target': 'B', 'value': 1}
    //   ]
    // };
    return dataGraph;
  }
}
